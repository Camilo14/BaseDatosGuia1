-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 20-05-2016 a las 01:23:23
-- Versión del servidor: 5.5.8
-- Versión de PHP: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `hospital`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asignacioncita`
--

CREATE TABLE IF NOT EXISTS `asignacioncita` (
  `IdAsignacionCita` bigint(20) NOT NULL AUTO_INCREMENT,
  `NombreAsignacionCita` varchar(50) NOT NULL,
  `FechaAsignacionCita` date DEFAULT NULL,
  `FKIdPaciente` bigint(20) NOT NULL,
  `FKIdOperario` bigint(20) NOT NULL,
  PRIMARY KEY (`IdAsignacionCita`),
  KEY `asignacionpaciente` (`FKIdPaciente`),
  KEY `asignacionoperario` (`FKIdOperario`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Volcar la base de datos para la tabla `asignacioncita`
--

INSERT INTO `asignacioncita` (`IdAsignacionCita`, `NombreAsignacionCita`, `FechaAsignacionCita`, `FKIdPaciente`, `FKIdOperario`) VALUES
(1, 'CitaPaciente3', '2016-05-19', 3, 2),
(2, 'CitaPaciente1', '2016-05-20', 1, 1),
(3, 'CitaPaciente2', '2016-05-21', 2, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detallehistorialpaciente`
--

CREATE TABLE IF NOT EXISTS `detallehistorialpaciente` (
  `IdDetalleHistorialPaciente` bigint(20) NOT NULL AUTO_INCREMENT,
  `NombreDetalleHistorialPaciente` varchar(50) NOT NULL,
  `FKIdHistorialPaciente` bigint(20) NOT NULL,
  `FKIdPaciente` bigint(20) NOT NULL,
  PRIMARY KEY (`IdDetalleHistorialPaciente`),
  KEY `detallehistorialpaciente` (`FKIdHistorialPaciente`),
  KEY `detallepaciente` (`FKIdPaciente`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Volcar la base de datos para la tabla `detallehistorialpaciente`
--

INSERT INTO `detallehistorialpaciente` (`IdDetalleHistorialPaciente`, `NombreDetalleHistorialPaciente`, `FKIdHistorialPaciente`, `FKIdPaciente`) VALUES
(1, 'CodMery1', 1, 2),
(2, 'CodMorgi2', 3, 1),
(3, 'Codhomer3', 1, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `diagnostico`
--

CREATE TABLE IF NOT EXISTS `diagnostico` (
  `IdDiagnostico` bigint(20) NOT NULL AUTO_INCREMENT,
  `NombreDiagnostico` varchar(50) NOT NULL,
  `FKIdDoctor` bigint(20) NOT NULL,
  PRIMARY KEY (`IdDiagnostico`),
  KEY `diagnosticodoctor` (`FKIdDoctor`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Volcar la base de datos para la tabla `diagnostico`
--

INSERT INTO `diagnostico` (`IdDiagnostico`, `NombreDiagnostico`, `FKIdDoctor`) VALUES
(1, 'muerte', 3),
(2, 'bienestar', 2),
(3, 'enfermo', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `doctor`
--

CREATE TABLE IF NOT EXISTS `doctor` (
  `IdDoctor` bigint(20) NOT NULL AUTO_INCREMENT,
  `NombreDoctor` varchar(50) NOT NULL,
  `DireccionDoctor` varchar(50) DEFAULT NULL,
  `TelefonoDoctor` varchar(50) DEFAULT NULL,
  `CorreoDoctor` varchar(50) DEFAULT NULL,
  `CargoDoctor` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`IdDoctor`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Volcar la base de datos para la tabla `doctor`
--

INSERT INTO `doctor` (`IdDoctor`, `NombreDoctor`, `DireccionDoctor`, `TelefonoDoctor`, `CorreoDoctor`, `CargoDoctor`) VALUES
(1, 'camilo', 'CR 73...', '2646139', 'c***@hotmail.com', 'Doctor'),
(2, 'cristhian', 'CR 72...', '2646137', 'cc***@hotmail.com', 'Doctorgefe'),
(3, 'freddy', 'CR 71...', '2646139', 'f***@hotmail.com', 'Doctorr');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historialpaciente`
--

CREATE TABLE IF NOT EXISTS `historialpaciente` (
  `IdHistorialPaciente` bigint(20) NOT NULL AUTO_INCREMENT,
  `NombreHistorialPaciente` varchar(50) NOT NULL,
  `FechaInicioHistorialPaciente` date DEFAULT NULL,
  `FechaFinalHistorialPaciente` date DEFAULT NULL,
  `FKIdDoctor` bigint(20) NOT NULL,
  PRIMARY KEY (`IdHistorialPaciente`),
  KEY `historialpacientedoctor` (`FKIdDoctor`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Volcar la base de datos para la tabla `historialpaciente`
--

INSERT INTO `historialpaciente` (`IdHistorialPaciente`, `NombreHistorialPaciente`, `FechaInicioHistorialPaciente`, `FechaFinalHistorialPaciente`, `FKIdDoctor`) VALUES
(1, 'felipe', '1999-01-06', '2016-05-19', 2),
(2, 'camilo', '1998-01-13', '2016-05-18', 3),
(3, 'freddy', '2000-04-06', '2016-05-20', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `medicamentos`
--

CREATE TABLE IF NOT EXISTS `medicamentos` (
  `IdMedicamentos` bigint(20) NOT NULL AUTO_INCREMENT,
  `NombreMedicamentos` varchar(50) NOT NULL,
  `FechaVencimientoMedicamentos` date DEFAULT NULL,
  `FKIdDoctor` bigint(20) NOT NULL,
  PRIMARY KEY (`IdMedicamentos`),
  KEY `medicamentodoctor` (`FKIdDoctor`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Volcar la base de datos para la tabla `medicamentos`
--

INSERT INTO `medicamentos` (`IdMedicamentos`, `NombreMedicamentos`, `FechaVencimientoMedicamentos`, `FKIdDoctor`) VALUES
(1, 'asetaminofen', '2016-05-19', 3),
(2, 'dolex', '2017-05-19', 2),
(3, 'durex', '2016-05-31', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `operario`
--

CREATE TABLE IF NOT EXISTS `operario` (
  `IdOperario` bigint(20) NOT NULL AUTO_INCREMENT,
  `NombreOperario` varchar(50) NOT NULL,
  `DireccionOperario` varchar(50) DEFAULT NULL,
  `TelefonoOperario` varchar(50) DEFAULT NULL,
  `CorreoOperario` varchar(50) DEFAULT NULL,
  `CargoOperario` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`IdOperario`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Volcar la base de datos para la tabla `operario`
--

INSERT INTO `operario` (`IdOperario`, `NombreOperario`, `DireccionOperario`, `TelefonoOperario`, `CorreoOperario`, `CargoOperario`) VALUES
(1, 'Fernando', 'Calle Mons', '4566889', 'CorreoDel@Live.com', 'Montas'),
(2, 'Fernan', 'Calle Mins', '656889', 'Correobel@Live.com', 'Medico cirujano'),
(3, 'Fer', 'Calle Muns', '874819', 'Correoall@gmail.com', 'Medico tratante de PT');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paciente`
--

CREATE TABLE IF NOT EXISTS `paciente` (
  `IdPaciente` bigint(20) NOT NULL AUTO_INCREMENT,
  `NombrePaciente` varchar(50) NOT NULL,
  `DireccionPaciente` varchar(50) DEFAULT NULL,
  `TelefonoPaciente` varchar(50) DEFAULT NULL,
  `CorreoPaciente` varchar(50) DEFAULT NULL,
  `FKIdDoctor` bigint(20) NOT NULL,
  PRIMARY KEY (`IdPaciente`),
  KEY `pacientecargo` (`FKIdDoctor`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Volcar la base de datos para la tabla `paciente`
--

INSERT INTO `paciente` (`IdPaciente`, `NombrePaciente`, `DireccionPaciente`, `TelefonoPaciente`, `CorreoPaciente`, `FKIdDoctor`) VALUES
(1, 'Monica', 'cra arriba', '2655448', 'Correo@hotmail.com', 1),
(2, 'Dilon', 'cra abajo', '25434788', 'Correo2@hotmail.com', 2),
(3, 'Moution', 'cra medio', '25644878', 'Correo3@hotmail.com', 3);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vistaasignacioncita`
--
CREATE TABLE IF NOT EXISTS `vistaasignacioncita` (
`IdAsignacionCitax` bigint(20)
,`FechaAsignacionCitax` date
);
-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `vistapacientecita`
--
CREATE TABLE IF NOT EXISTS `vistapacientecita` (
`IdPacientex` bigint(20)
,`NombrePacientex` varchar(50)
,`Pacientex` bigint(20)
);
-- --------------------------------------------------------

--
-- Estructura para la vista `vistaasignacioncita`
--
DROP TABLE IF EXISTS `vistaasignacioncita`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vistaasignacioncita` AS select `asignacioncita`.`IdAsignacionCita` AS `IdAsignacionCitax`,`asignacioncita`.`FechaAsignacionCita` AS `FechaAsignacionCitax` from `asignacioncita`;

-- --------------------------------------------------------

--
-- Estructura para la vista `vistapacientecita`
--
DROP TABLE IF EXISTS `vistapacientecita`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `vistapacientecita` AS select `paciente`.`IdPaciente` AS `IdPacientex`,`paciente`.`NombrePaciente` AS `NombrePacientex`,`paciente`.`FKIdDoctor` AS `Pacientex` from `paciente`;

--
-- Filtros para las tablas descargadas (dump)
--

--
-- Filtros para la tabla `asignacioncita`
--
ALTER TABLE `asignacioncita`
  ADD CONSTRAINT `asignacioncita_ibfk_1` FOREIGN KEY (`FKIdPaciente`) REFERENCES `paciente` (`IdPaciente`),
  ADD CONSTRAINT `asignacioncita_ibfk_2` FOREIGN KEY (`FKIdOperario`) REFERENCES `operario` (`IdOperario`);

--
-- Filtros para la tabla `detallehistorialpaciente`
--
ALTER TABLE `detallehistorialpaciente`
  ADD CONSTRAINT `detallehistorialpaciente_ibfk_1` FOREIGN KEY (`FKIdHistorialPaciente`) REFERENCES `historialpaciente` (`IdHistorialPaciente`),
  ADD CONSTRAINT `detallehistorialpaciente_ibfk_2` FOREIGN KEY (`FKIdPaciente`) REFERENCES `paciente` (`IdPaciente`);

--
-- Filtros para la tabla `diagnostico`
--
ALTER TABLE `diagnostico`
  ADD CONSTRAINT `diagnostico_ibfk_1` FOREIGN KEY (`FKIdDoctor`) REFERENCES `doctor` (`IdDoctor`);

--
-- Filtros para la tabla `historialpaciente`
--
ALTER TABLE `historialpaciente`
  ADD CONSTRAINT `historialpaciente_ibfk_1` FOREIGN KEY (`FKIdDoctor`) REFERENCES `doctor` (`IdDoctor`);

--
-- Filtros para la tabla `medicamentos`
--
ALTER TABLE `medicamentos`
  ADD CONSTRAINT `medicamentos_ibfk_1` FOREIGN KEY (`FKIdDoctor`) REFERENCES `doctor` (`IdDoctor`);

--
-- Filtros para la tabla `paciente`
--
ALTER TABLE `paciente`
  ADD CONSTRAINT `paciente_ibfk_1` FOREIGN KEY (`FKIdDoctor`) REFERENCES `doctor` (`IdDoctor`);
